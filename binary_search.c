#include<stdio.h>
#include<stdlib.h>
#define MAX 20

int binary_search(int a[MAX], int low, int high, int x){
  int mid;

  if(high >= low){
    mid = (low + high)/2;

    if(x == a[mid]){
      return mid;
      }

      if(x < a[mid]){
	return binary_search(a, low, mid-1, x);
      }
      else{
	return binary_search(a, mid+1, high, x);
      }
  }
  return -1;
}

int main(void){
  int a[MAX], n, x;
  printf("enter the size of array\t");
  scanf("%d", &n);

  printf("enter the elements in ascending order\n");
  for(int i = 0; i < n; i++){
    scanf("%d", &a[i]);
  }
  printf("enter the element u want to search:\t");
  scanf("%d", &x);
    int result = binary_search(a, 0, n-1, x);
    if(result == -1){
      printf("Element not found\n");
    }
    else{
      printf("Element found at index: %d", result);
    }
} 
    
